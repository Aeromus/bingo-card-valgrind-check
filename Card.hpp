#ifndef BINGO_CARD_H
#define BINGO_CARD_H


#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <vector>


class Card {

private:

    int cardSize;
    int max;
    std::vector<int> usedNums;
    std::vector<std::vector<int>> vectorArray;

public:

    Card();
    Card(int size, int max);

    void printCard();
    void deleteCard();
    int getCardSize();
    int getNumMax();
};


#endif //BINGO_CARD_H