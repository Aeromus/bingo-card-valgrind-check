//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_DECK_H
#define BINGO_DECK_H

#include <ostream>
#include <vector>
#include "Card.hpp"
// TODO: Extend this definition as you see fit

class Deck {
    
private:
    std::vector<Card> playDeck;
    int deckSize;

public:
    Deck(int cardSize, int cardCount, int numberMax);
    ~Deck();

    void print(std::ostream& out);
    void print(std::ostream& out,unsigned int cardIndex);
    void deleteDeck();
    int getDeckSize();
    std::vector<Card> getDeckVector();
};

#endif //BINGO_DECK_H
