
#include "Card.hpp"

Card::Card(int size, int num_Max)
{
    std::vector<std::vector<int>>  playCard;
    cardSize = size;
    max = num_Max;

    if(size < 1 || num_Max < 1)
        exit(-1);

    for(int i = 0; i < size; i++) {

        std::vector<int> rowVector;

        for (int ii = 0; ii < size; ii++) {

            int tempNum = (rand() % num_Max + 1);
            while(std::find(usedNums.begin(), usedNums.end(), tempNum)  != usedNums.end())
            {
               // std::cout<< "Duplicate Number Generated\nGenerating New Number...\n";
                tempNum = (rand() % num_Max + 1);
            }

            rowVector.push_back(tempNum);
            usedNums.push_back(tempNum);
        }

        playCard.push_back(rowVector);
    }
     
      
     
    vectorArray = playCard;
}


Card::Card(){}

void Card::printCard() {

    for (unsigned int i = 0; i < vectorArray.size(); ++i)
    {


        for (unsigned int k = 0; k < vectorArray[i].size(); k++) {
            if(k == vectorArray[i].size() -1)
                std::cout<< "+----+";

            else
            std::cout << "+----";
        }
        std::cout << std::endl;

        for (unsigned int j = 0; j < vectorArray[i].size(); ++j) {

            //std::cout <<  "+----+\n| " << vectorArray[i][j] << " |\n+----+";
            if(vectorArray[i][j] < 9 && j == vectorArray[i].size() - 1)
            std::cout << "|  " << vectorArray[i][j] << " |";

                else if (vectorArray[i][j] < 10)
                std::cout << "|  " << vectorArray[i][j] << " ";

                else if(vectorArray[i][j] > 9 && j ==vectorArray[i].size() - 1)
                std::cout << "| " << vectorArray[i][j] << " |";


            else
                std::cout << "| " << vectorArray[i][j] << " ";

        }
        std::cout << std::endl;

    }

    for (int i = 0,  max =  vectorArray[0].size(); i <= max; i++)
    {
        if( i == max-1)
            std::cout << "+----+" ;
        else if (i !=max)
            std::cout << "+----";
    }
    std::cout << std::endl;


}

void Card::deleteCard(){



   for (unsigned int i = 0; i < vectorArray.size(); ++i) {

        for (unsigned int j = 0; j < vectorArray[i].size(); ++j) {
            vectorArray[i].erase(vectorArray[i].begin(), vectorArray[i].end());
        }

        vectorArray.erase(vectorArray.begin(),vectorArray.end());
    }
    vectorArray.clear();

}

int Card::getCardSize() {

    return cardSize;
}

int Card::getNumMax(){

    return max;
}