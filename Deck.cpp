//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.hpp"

Deck::Deck(int cardSize, int cardCount, int numberMax)
{
    if(cardSize < 3 ){
        std::cout << "Invalid Card Size Input";
        return;
    }
    
    deckSize = cardCount;

    //Create Deck of n Cards
    for (int i = 0; i  < cardCount; i++)
    {
       playDeck.push_back(Card(cardSize, numberMax));
    }
}

Deck::~Deck()
{
    for (unsigned int i = 0; i < playDeck.size(); ++i) 
    {
        playDeck[i].deleteCard();
    }

        playDeck.erase(playDeck.begin(),playDeck.end());
}

void Deck::print(std::ostream& out)
{
    for (unsigned int i = 0; i < playDeck.size(); ++i) 
    {
        std::cout << "Card Number: " << i + 1<< std::endl;
        playDeck[i].printCard();
        std::cout << std::endl;
    }
}

void Deck::print(std::ostream& out, unsigned int cardIndex)
{
    if(cardIndex < 1 || cardIndex > playDeck.size()+1)
    {
        std::cout << "Card Index out of Bounds\n";
        return;
    }
    
    else {
        std::cout << "Card Number: " << cardIndex  << std::endl;
        playDeck[cardIndex - 1].printCard();
    }
}

void Deck::deleteDeck(){
    
    for(unsigned int x = 0; x < playDeck.size(); x++)
    {
        playDeck[x].deleteCard();
    }
    
    playDeck.clear();
    
    std::cout<<"*****Deck Deleted*****\n";
}


